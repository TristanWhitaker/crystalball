package android.whitakert.crystalball;

import android.media.MediaPlayer;

import java.util.Random;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will NEVER come true.",
                "Im tired, ask me later",
                "Probably not.",
                "Definitively not.",
                "For sure."
        };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        int rnd = new Random().nextInt(6);
        return answers[rnd];
    }
}